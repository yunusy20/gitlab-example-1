---
layout: post
title: "Gitlab Pages ile Sitenizi Yayına Alın"
date: 2020-03-16 21:00:00 +0300
categories: Gitlab
---

Bu yazımızda sizler ile birlikte hazırladığımız bir html sayfayı gitlab üzerinden yayına alacağız. Gitlab'ın bu hizmetinin adı **Gitlab Pages**'dır. Gitlab CI hakkında sizlere [bu linkte](https://www.muhammetdilmac.com.tr/2020/02/gitlab-deponuzda-gitlab-ci-i-yapilandirmak) ki yazımda bilgi vermiştim. Gitlab Pages'da zaten Gitlab CI'ın devamı niteliğindedir. Gitlab Pages bizlere html outputu alıp yayınlamaktadır. Belirttiğim üzere Gitlab CI'ı kullandığı için Gitlab Pages servisi içinde .gitlab-ci.yml dosyasını yapılandırmamız gerekmektedir. Gitlab CI yapılandırma dosyamız içinde eğer Gitlab Pages'ı kullanmak istiyorsak **pages** adımı altında yapmak istediğimiz işlemleri yazmamız gerekmektedir. Buna ek olarak pages adımının altında özel bir tanım olarak artifacts etiketini kullanmaktayız. Bu etiketin altında paths anahtarı altında dışarı çıkartmak istediğimiz dizini belirtmeliyiz. Bu bilgiler ışığında isterseniz Jekyll ile hazırladığımız bir siteyi yayına alalım. Bu örneklendirme sırasında sizin tarafınızdan jekyll ile hazırlanmış bir siteniz olduğu varsayılmaktadır. Jekyll depomuzun içinde gitlab ci'ı çalıştırabilmek için `.gitlab-ci.yml` adında bir dosya oluşturuyoruz. Sonrasında Gitlab Pages için gerekli tüm direktifleri içine açıklamarıyla birlikte ekleyelim.

1. İlk olarak tekrar ihtiyaç duyduğumuz ortamı belirtmemiz gerekiyor. Bunun için `image` etiketini kullanıyoruz. Jekyll için ihtiyaç durduğumuz ortam `ruby:2.6`.
2. Sonrasında pages adımını eklememiz gerekli tabi bunun altında da gerekli etiketleri eklememiz gerekecek.
   1. **script** etiketinin altında adım adım hangi komutların çalışmasını istiyorsan onları eklemeliyiz. 
   2. **artifacts** etiketinin altında **paths** etiketini ekleyip onunda altına script'ler sonucu oluşan dizinlerden hangisinin yayına alınacağını belirtmeliyiz.

Bu bilgiler ışında ortaya çıkan .gitlab-ci.yml dosyamız şu şekilde olacaktır;
```yml
image: ruby:2.6

pages:
  script:
    - gem install bundler:2.1.2
    - bundle install
    - bundle exec jekyll build -d public
  artifacts:
    paths:
    - public
```

Script komutları sırası ile;
1. Bundler'ın 2.1.2 versiyonunu kurduruyoruz. 
2. Jekyll için gerekli kütüphaneleri yüklüyoruz.
3. Jekyll ile oluşturduğumuz siteyi public dizini altına çıktı olarak istiyoruz.

artifacts'de paths altında jekyll'ın çıktısını yayına alıyoruz. 

Tüm bu tanımlardan sonra CI / CD altında Pipelines'da bir pipeline'a tıklayınca şöyle bir şey görmeniz gerekmektedir.
![.gitlab-ci.yml dosyasının kaydından sonra oluşan pipeline](/assets/gitlab-pages-ile-sitenizi-yayina-alin/1-pipeline.png)

Burada tek adım olarak gözükmektedir fakat bu tek adım tamamlandıktan sonra gitlab otomatik olarak deployment adımını ekleyecektir ve şu şekilde gözükecektir.
![Pages aşaması tamamlanınca pages:deploy aşaması gözükecektir.](/assets/gitlab-pages-ile-sitenizi-yayina-alin/2-after-complete.png)

Eğer işin detayına girerseniz işin sonucu çıkartılmasını istediğiniz dizine bir link göreceksiniz. Buna iş çıktısının sağında Job Artifacts altında browse'den erişebilirsiniz.
![İş detayından iş çıktılarına ulaşma](/assets/gitlab-pages-ile-sitenizi-yayina-alin/3-job-detail.png)

Biz artifacts etiketinin altında bulunan paths etiketine sadece public dizinini eklediğimiz için bu sayfada sadece public dizinini göreceğiz.
![Artifact dizinlerini görüyoruz](/assets/gitlab-pages-ile-sitenizi-yayina-alin/4-artifact-index.png)

Public dizinin içine tıkladığımız zaman ise jekyll'in oluşturduğu çıktıyı görebiliriz.
![Jekyll'ın output'u olan siteyi görüyoruz](/assets/gitlab-pages-ile-sitenizi-yayina-alin/5-public-directory.png)

Son olarak sitemizin adresini Settings'de Pages altında görebiliriz. Eğer sitemizin farklı bir adrestede gözükmesini istiyorsak buraya bunu eklememiz gerekmektedir. 
![Oluşturduğumuz sitemizin çıktısının olduğu adres](/assets/gitlab-pages-ile-sitenizi-yayina-alin/6-site-address.png)

## Farklı Bir Domain Adresi Kullanma
Eğer sitenizi gitlab.io domain adı dışında birşey ile yayına almak isterseniz Settings'de Pages altında New Domain'e tıklamanız gerekmektedir. Ardından karşınıza gelecek ekranda Domain alanına kullanmak istediğiniz domaini tam olarak yazınız. 
![Settings Pages Add New Domain Butonuna tıklanınca gelen ekran](/assets/gitlab-pages-ile-sitenizi-yayina-alin/7-add-new-domain.png)

Sonra ki ekranda domaini doğrulamanız için eklemeniz gereken TXT kaydını göreceksiniz.
![Domaini doğrulama ekranı](/assets/gitlab-pages-ile-sitenizi-yayina-alin/8-dns-information.png)

Bu TXT kaydını belirtilen şekilde DNS yöneticinize ekledikten sonra domain adresini CNAME kaydı oluşturarak io'lu domain adresinizi yazmanız şekilde ki gibi yeterli olacaktır.
![DNS kayıtları ekranı](/assets/gitlab-pages-ile-sitenizi-yayina-alin/9-dns-records.png)

## Not
Sitenizi ilk defa eğer yayına aldıysanız sitenizin ayağa kalması yaklaşık olarak 30 dakika sürebilir. Yaptığınız değişikliklerin ise uygulanması süreç bittikten sonra yaklaşık olarak 5-10 dakika sürebilmektedir.

## Kaynaklar
- [Gitlab Pages Dokümantasyonu](https://docs.gitlab.com/ee/user/project/pages/)
- [Jekyll](https://jekyllrb.com/)