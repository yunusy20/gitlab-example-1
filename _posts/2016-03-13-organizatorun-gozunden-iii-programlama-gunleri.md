---
date: 2016-03-13 16:07:02 +0300
categories: "Organizasyon"
title: "Organizatörün Gözünden III. Programlama Günleri"
layout: post
---
<p>Karabük Üniversitesi Bilişim Teknolojileri Kulübünde başkan yardımcısı iken başlatmış olduğum Programlama Günleri 
etkinliğinin
bu sene üçüncüsünü düzenledik. Öncelikli olarak etkinlikte konuşmacılarımıza, sponsorlarımıza ve ekip arkadaşlarıma çok
teşekkür ediyorum emekleri için. III. Programlama Günleri’ni gözümde en kıymetli yapan şey şüphesiz ki salonda Trabzon,
İstanbul, Ankara, Bolu, Kastamonu, Aydın gibi bir çok ilden gelen konuk dinleyicilerimizin olmasıydı hatta salonun genelini
dolduranlar il dışından gelenlerdi ne kadar garip değil mi? :) Bu etkinlik sayesinde belkide yıllardır bir araya gelemeyen
konuşmacıları bir araya gelmeleri için fırsat olmuş gibi hissetmedim değil aslında :) Her konuşmacının karşılaşma anında sanki
Sinan Çetin’in programında açılan kapı sırasında çalan müziği duyar gibi oluyordum :)</p>
<div class="iframe-video">
<iframe width="560" height="315" src="https://www.youtube.com/embed/J6GDypQvNOI" frameborder="0" allowfullscreen=""></iframe>
</div>
<p>Eee bu kadar güzel insanları böyle bir araya getirirsin de sohbetler nasıl olur? Tabi ki bol feyizli sohbetler :) Her akşam
konukların yanlarında sohbetlerini dinleme şansına sahip olduğum için gerçekten çok şanslıydım. Her cümleden farklı bir ders
ve feyiz kıyordu tabi toplamasını bilene :P Bende elimden geldiğince bunlardan faydalanmaya ve bir şeyler kapmaya çalıştım.
Bunlar akşamları dönen olaylardı tabi… Şimdi gel gelelim etkinliğe, etkinlik gününün gecesinde ancak Ankara’ya varabildim.
Gökmen ile birlikte gelmiştik İsa bizi terminalden almaya gelmişti. Tam araç ile otele geçerken farkettik ki Uğur Özyılmazel,
Burak Aydın ve Emir Karşıyakalı’nın araçları arkamızda ki araçmış :) Fatih Arslan, Üstün Özgür ve Fırat Küçük bizden önce
otele geçmişlerdi zaten. Akşam yemek yemeden yola çıktığımız için midemiz kazınmıyor değildi :) Uğur abiler ile birlikte gece
gece birşeyler aramaya başladık ki tam o sırada bir çorbacı gözümüze çarptı ve çorbacıya gittik(gerçekten muhteşemdi
kesinlikle tavsiye ederim sizede. Safranbolu Merkez’den Eski çarşıya doğru giderken merkezin çıkışında sağda kalıyor.) İlk
günümüz de Üstün Özgür’ün sunumu ile başladık kendisine teşekkür belgesini ben takdim ettim, ardından programımız Fırat
Küçük’ün eğlenceli ve bir o kadar samimi sunumu ve sırası ile Güray Yıldırım, Fatih Erikli, Gökmen Güreşçi ve Daron Yöndem’in
sunumu ile devam etti. Ne yazık ki etkinlikden önce ki gün Necdet Yücel hocamız rahatsızlanması nedeni ile katılamamıştı ve
onların programı Aybüke ve Gülşah ile birlikte iptal oldu. Daron Yöndem’in sunumu çok ilginç oldu :) Güzel itiraflar aldık
Microsoft tarafından :P Yalnız asıl bomba olan ise konu ile alakasız gelen bir çok soru insanlar bildiğiniz Daron Yöndem gelse
de Microsoft ile ilgili tüm sorularımızı sorsak diye beklemişler(xbox, asp, azure vs.) :) İlk gün bu şekilde geçtikten sonra
tabi ki yemek maceralarımız var :) İkinci günde ise Fatih Arslan, Fatih Kadir Akın, Emir Karşıyakalı, Tayfun Öziş Erikan, Uğur
Özyılmazel, Gökmen Göksel, Serdar Doğruyol, Lemi Orhan Ergin, Burak Yetgin, Halil İbrahim Nuroğlu, Emre Efendioğlu ile devam
etti. İkinci gün ki sunumlar arasında şüphesiz ki benim gözümde en efsane olan Lemi abinin git sunumuydu gerçekten bir fırtına
gibi esti geçti hayranlık kaldı geride… :) Üçüncü günümüzde ise workshoplar vardı bugün baya bir koşturmacılı geçti zira son
gün bir pizza ayarlama olayımız olmuştu :) Amacımız workshop’lara katılan tüm katılımcılara pizza ikram etmekti güzel bir
pazarlık ile bunuda sağladık :) Etkinlik oldukça güzeldi ve etkinleyiciydi(tabi feyz almasını bilene :P) Her etkinlikte olduğu
gibi bu etkinlikte de en kötü an konukları yolcu etmekti o kadar değerli insanı tek tek yolcu etmek oldukça zor olmuştu zira
daha öğrenilmesi gereken çok şey vardı… Bunlar etkinlik hakkında genel notlardı biraz da kendime çıkarttığım notları sizin
ile paylaşmak istiyorum.</p>
<ol>
<li>Konuşmacılara verilen sürülerin hatırlatması için sunumun hemen öncesinde ve maillerde kullanıcıya süresi hatırlatılmalı,
ayrıca son 5dk 10dk gibi hatırlatıcı tabelalar olmalı.</li>
<li>Konuşmacıların konaklayacakları otellere dikkat et direk konukların odalarını rezerve et ve odaları kontrol et!</li>
<li>Sunumlar arasında ki araları 15dk olarak ayarla ki insanlar konuklarla sohbet edebilsin ve salon havalansın.</li>
<li>Sunum aralarında verdiğin ikramları daha çeşitli tutmalısın.</li>
<li>Sunumlar bittikten sonra konuklarla yapacağın şeyleri önceden kararlaştır ve konuklara bunları bildir, ilgili yerlerde
gerekli rezervasyonları yap.</li>
<li>Etkinlik duyurularını sınıflarda da yap sadece internet üzerinden yapınca biz duymadık bahaneleri ile karşılaşma :)</li>
<li>En az iki adet kamera ayarla çekimler için!</li>
<li>En az iki adet fotoğraf makinası ayarla resim çekimleri için!</li>
<li>Mümkünse ekip sana yakın kişiler olsun en azından seni tanıyanlar ki garip garip tripler ile karşılaşma.</li>
</ol>
<p>Bunlar başlıca maddeler eğer etkinlik düzenleyecek iseniz aman siz bu dediklerimi yapın emin olun ki etkinliğin keyfi en az 
90
kat artacak :) Bu gereksiz yazıyı okuduğunuz için teşekkürler :)</p>
<p>Şu resmi koymaz isem olmaz :)</p>
<figure>
<img src="/assets/organizator-gozunden-iii-programlama-gunleri/1.jpg" alt="III. 
Programlama Günleri II. Gün Sonu Resmi">
<figcaption>III. Programlama Günleri II. Gün Sonu Resmi</figcaption>
</figure>
<ul>
<li><strong>Dipnot:</strong> Etkinlik süresince şüphesiz ki en çok tebriği hak eden; <strong>Cihan Şahin</strong>, 
<strong>İlim Betül Yavuz</strong> ve <strong>İsa
Kuran</strong>‘dır. Kendilerine buradan da verdikleri emekler için teşekkür ediyorum.<br></li>
<li><strong>Dipnot:</strong> Etkinliğimize ilk dakikalarda sponsor olan <strong>PayU</strong> ve <strong>Bilio</strong>‘ya 
ardından <strong>HTMLMag</strong>‘e, <strong>İndir</strong>‘e,
<strong>Yazılım Parkı</strong>, <strong>Hidayet Doğan</strong>‘a, <strong>adphorus</strong>‘a, <strong>API PLUG</strong>‘a ve 
son dakika golu ile bizi gerçekten sevindiren <strong>Protel</strong>‘e
verdikleri destekler için gerçekten çok teşekkürler. Böyle firmalar iyi ki varlar. Karşılıksız olarak bu tarz etkinliklere 
destek
olan firmalar çok az eğer III. Programlama Günleri’ni yapabildiysek bunun sebebi bu firmalardır :)<br></li>
<li><strong>Dipnot:</strong> Kırmayıp gelmeyi kabul eden tüm konuklarımıza ayrıca çok teşekkür etmek istiyorum.</li>
<li><strong>Dipnot:</strong> Bu üniversitede en başından beri yapılan etkinliklere en çok destek veren sayın <strong>İsmail 
Rakıp Karaş</strong> hocama da
ayrıca çok teşekkür ediyorum.</li>
<li><strong>Dipnot:</strong> Kendime de teşekkür ediyorum zira o kadar işimin içinde bir şekilde zaman ayırıp bu etkinliği 
düzenleyebildiğim
için :) Benim bu durumda bu etkinliğin organizasyonunda bu kadar aktif rol almam demek kimsenin zaman ayıramıyorum demesine en
büyük tezat durumdur :) [Kendimi de övdüm o zaman ben gidiyorum :)]<br></li>
</ul>
