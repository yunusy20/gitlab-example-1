---
date: "2016-03-24 16:02:36 +0300"
categories: "Güvenlik"
title: "Ruby on Rails Web Çatısı ve Güvenliği"
layout: post
---
<p>26 Mart 2016’da <strong>Özgür Yazılım ve Linux Günleri</strong> içinde düzenlenen <strong>Uygulama Güvenliği</strong> 
günleri bünyesinde <strong>Ruby on Rails
Web Çatısı ve Güvenliği</strong> konulu bir sunum yaptım. Sunumu hafta içi yaşadığım iş yoğunluğu nedeni ile ancak sunumdan 
1-2 saat
öncesinde hazırlayabildim :) Bunun için herkesden özür dilerim…<br>
</p><figure>
<img src="/assets/ozgur-yazilim-ve-linux-gunleri/1.jpg" alt="Özgür Yazılım ve Linux 
Günleri'nde yapmış olduğum Ruby on Rails Web Çatısı ve Güvenliği konulu sunumdan bir kare">
<figcaption>Özgür Yazılım ve Linux Günleri’nde yapmış olduğum Ruby on Rails Web Çatısı ve Güvenliği konulu sunumdan bir 
kare</figcaption>
</figure><p></p>
<p>Sunumu hazırlama nedenim frameworklere yeni başlıyanları biraz bilinçlendirmekti. Konu Rails olmasına rağmen hatalar hep
aynıdır çünkü… Spesifik konulara girmek istemedim zira gelen kişilerin teknik olarak ne derece olacaklarını az çok
kestiriyordum. Ayrıca Türkiye’de güvenlik algısı daha çok klasik php ve klasik asp üzerinde araştırılan bir algı ne yazık ki
biraz da buna el atmak istedim açıkçası… Güvenlik sadece pure yazılan yazılımlarda değil frameworklerde de olan ve
araştırılması gereken bir konudur :)
</p><figure>
<img src="/assets/ozgur-yazilim-ve-linux-gunleri/2.jpg" alt="Özgür Yazılım ve Linux 
Günleri'nde yapmış olduğum 
Ruby on Rails Web Çatısı ve Güvenliği konulu sunumdan bir kare">
<figcaption>Özgür Yazılım ve Linux Günleri’nde yapmış olduğum Ruby on Rails Web Çatısı ve Güvenliği konulu sunumdan bir 
kare</figcaption>
</figure><p></p>
<p>Diyip fazla uzatmadan sunumun linkini paylayaşım. Sunuma buradan ulaşabilirsiniz; <a 
href="https://muhammetdilmac.com.tr/files/ozgur-yazilim-ve-linux-gunleri-2016/index.html" target="_blank" rel="noopener noreferrer">Ruby on Rails Web
Çatısı ve Güvenliği | Özgür Yazılım ve Linux Günleri’16</a></p>
